<?php

class Authentication{

	private static $instance=null;
	private $private_page;
	private $public_page;
	public function __construct(){
		$public_page = Helperfunction::get_page_data('public');
		$private_page =Helperfunction::get_page_data('private');
		//public page
		$this->login_page_id = $public_page['login_page'] ;
		$this->register_page_id=$public_page['register_page'];

		//private page
		$this->hair_units_id=$private_page['hair_units'];

		add_action('wp_head',array($this, 'header_assets'));
		add_action('after_setup_theme', array($this, 'remove_admin_bar'));
		add_action( 'wp_login_failed', [$this,'login_fail'] );//login failed
		add_action('authenticate', array($this, 'check_username_password'), 1, 3);//check email
		add_action('template_redirect', array($this, 'template_redirects'));//template redirect
		add_filter( 'login_redirect', [$this,'wpdocs_my_login_redirect'], 10, 3 );
		//registration process
		add_action('wp_ajax_registration_process',[$this,'register_process']);
		add_action('wp_ajax_nopriv_registration_process',[$this,'register_process']);

	}
	public static function get_instance(){
		if(!self::$instance || self::$instance == null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function header_assets() {
		echo '<script>var ajax_url = "'.admin_url('admin-ajax.php').'" </script>';
	}

	public function remove_admin_bar(){
		if (current_user_can('subscriber')) {
			show_admin_bar(false);
		}
	}

	
	public function wpdocs_my_login_redirect( $url, $request, $user ) {
		if ( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
			if ( $user->has_cap( 'administrator' ) ) {
				$url = admin_url();
			} else {
				$url =  get_page_link($this->hair_units_id);
			}
		}
		return $url;
	}
	public function login_fail( $username) {
		
		$referrer = $_SERVER['HTTP_REFERER'];
		if(empty($username)) {
			wp_safe_redirect(get_the_permalink($this->login_page_id).'?login=failed');
			exit();
		}
		else if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')     ) {
			wp_redirect( $referrer . '?login=failed' );
			exit;
		}
	}

	public function check_username_password( $login, $username, $password ) {
		$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
		if( !empty( $referrer ) && !strstr( $referrer,'wp-login' ) && !strstr( $referrer,'wp-admin' ) ) { 
			if( $username == "" || $password == "" ){
				wp_safe_redirect(get_the_permalink($this->login_page_id).'?login=failed');
				exit;
			}
		}
	}
	 public function template_redirects() {  
    	if(is_404()) return;
    	global $post;
    	if(is_null($post)) return;
    	if(is_customize_preview()) return false;
    	$current_post_id = $post->ID;
    	// other redirection 
    	$protected_ids = [
    		$this->hair_units_id,
    		
    	];
    	if(isset($current_post_id)) {
    		if(in_array($current_post_id, $protected_ids)){
    			if(!is_user_logged_in()){
    				wp_redirect(get_permalink($this->login_page_id)); 
    				exit;
    			}

    		}
    		if($current_post_id==$this->login_page_id || $current_post_id ==$this->register_page_id){
    			if(is_user_logged_in()){
    				wp_redirect(get_permalink($this->hair_units_id)); 
    				exit;
    			}
    		}
    	}
    	else {
    		if(!is_user_logged_in()) {
    			wp_redirect(get_permalink(home_url())); 
    			exit;
    		}
    	}
    }


    public function register_process(){
		if (!isset( $_REQUEST['form_nonce_registration'] ) || !wp_verify_nonce( $_REQUEST['form_nonce_registration'], 'form_nonce_registration' )) 
			wp_send_json(array('type' => 'error', 'message' => __('Something went wrong', 'divi')));
		$data = $_REQUEST['register_meta'];
		$error = new WP_Error;
		$html  = '';
		if(empty($first_name = $data['first_name'])) {
			$error->add('empty_first_name', __('First Name is required'));
		}
		if(empty($last_name = $data['last_name'])) {
			$error->add('empty_last_name', __('Last Name is required'));
		}
		if(empty($user_login=$data['user_login'])){
			$error->add('empty_user_login', __('Username is required'));
		}
		if(!is_email($data['user_email'])) {
			$error->add('email_invalid', __('Email is invalid'));
		}
		if(email_exists($data['user_email'])) {
			$error->add('email_exists', __('Email already exitst'));
		}
		if(empty(esc_attr($data['password']))) {
			$error->add('empty_password', __('Password is empty'));
		}
		if(empty(esc_attr($data['confirm_password']))) {
			$error->add('empty_confirm_password', __('Confirm Password is empty'));
		}
		if(esc_attr($data['password']) != esc_attr($data['confirm_password'])){
			$error->add('password_mismatch', __('Confirm Passwords do not match'));
		}
		if($error_messages = $error->get_error_messages()) {
			$html .='<ul>';
			foreach ($error_messages as $message) {
				$html .='<li>'.$message.'</li>';
			}
			$html .='</ul>';
			wp_send_json(array('type' => 'error', 'message' => $html));
		}else{
			$email = esc_attr($data['user_email']);
			$userdata = array(
				'first_name' => $first_name,
				'last_name'  => $last_name,
				'user_login' => $user_login,
				'user_email'  => $email,
				'user_pass'  => $data['password'],
				'user_registered'	=> date('Y-m-d H:i:s'),
				'role'=>'subscriber'
			);

			$user_id = wp_insert_user( $userdata ) ;
			$html='Thank you for creating an account.';
			wp_send_json(array('type' => 'success', 'message' =>$html));
		}
	}

	/*private function new_user_welcome_email($user_id){
		$subject    = __('Account Registration');
		$headers    = array('Content-Type: text/html; charset=UTF-8');
		$user       = get_user_by('id', $user_id);
		$username = $user->data->user_login;
		$email      = $user->data->user_email;
		ob_start();
		include_once(get_stylesheet_directory() . '/includes/templates/register_welcome_email.php');
		$body = ob_get_contents();
		ob_end_clean();
		wp_mail( $email, $subject, $body, $headers );
	}*/
	
}
Authentication::get_instance();