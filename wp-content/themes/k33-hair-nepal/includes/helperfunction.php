<?php

class Helperfunction{
	/**
	 * @return list of page id
	 */
	public static function pageId(){
		return [
			'aboutUs_id' =>17
		];
	}
	/**
	 * @return list of branchs
	 */
	public static function getBranchsPageId(){
		return [
			'chabahil'=>126,
			'koteshwor'=>129,
			'tripureshowor'=>131,
		];
	}
	/** 
	 * @return list of contact
	 */
	public static function getContactInfo(){
		return [
			'phone'=>get_theme_mod("phone"),
			'email'=>get_theme_mod('email'),
			'location'=>get_theme_mod('location')
		];

	}
	/** 
	 * @return social link
	 */

	public static function getSocialLinks(){
		return[
			'facebook'=>get_theme_mod('facebook_link'),
			'youtube'=>get_theme_mod('youtube_link'),
			'instagram'=>get_theme_mod('instagram_link')
		];
	}

	/**
	 * @return destination list
	 */
	public static function getDestinations(){
		return get_theme_mod('destinations');
	}
	/**
	 * @return list of banners
	 */
	public static function getBanners(){
		return get_theme_mod('banners');
	}
	/**
	 *  @return inner page banner image
	 **/
	public static function getInnerPageBanner(){
		return get_theme_mod('inner_page_banner_image');
	}

	/**
	 * @return aboutUs
	 */
	public static function getAboutus(){
		$aboutId=self::pageId()['aboutUs_id'];
		return get_post($aboutId);
	}

	/**
	 * @return gallery
	 */
	public static function getGalleries(){
		return get_theme_mod('galleries');
	}


	public static function get_page_data($type){
		switch($type){
			case 'public':
				return [
					'login_page'=>60,
					'register_page'=>122
				];
			break;
			case 'private':
				return[
					'hair_units'=>116,
				];
			break;
			default:
				return[
				];
			break;
		}
		return [];
	}


	/**
	 * @return chabahil location details
	 */
	public static function getBranch($id){
		return get_post($id);
	}
	

}

?>