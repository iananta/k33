<?php
class Extras{
	public static $instance=null;
	public $template_dir_uri;
	public $stylesheet_dir_uri;
	public $stylesheet_dir;
	public function __construct(){
		$this->template_dir_uri   =  get_template_directory_uri();
		$this->stylesheet_dir_uri =  get_stylesheet_directory_uri();
		$this->stylesheet_dir     =  get_stylesheet_directory();
		//hooks
		add_action( 'wp_enqueue_scripts',[$this,'theme_enqueue_styles']);
		add_action( 'after_setup_theme', [$this,'theme_support']);
		add_action('init',[$this,'register_menus']);
		add_filter('the_content',[$this,'clear_br']);
	}
	public static function get_instance() {
		if(!self::$instance || self::$instance == null) {
			self::$instance = new self();
		}
		return self::$instance;
	} 

	/**
	* register script and style and enqueue it
	*
	* @return null.
	*/

	public function theme_enqueue_styles(){
		//load style
		wp_enqueue_style('fontawesome-css',$this->stylesheet_dir_uri.'/assets/plugins/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('animate-css',$this->stylesheet_dir_uri.'/assets/css/animate.min.css');
		wp_enqueue_style('setting-css',$this->stylesheet_dir_uri.'/assets/css/setting.css');
		wp_enqueue_style("bootstrap-css",$this->stylesheet_dir_uri.'/assets/plugins/bootstrap/css/bootstrap.min.css');

		wp_enqueue_style("owl-css",$this->stylesheet_dir_uri.'/assets/plugins/OwlCarousel/dist/assets/owl.carousel.min.css');
		wp_enqueue_style("owldefault-css",$this->stylesheet_dir_uri.'/assets/plugins/OwlCarousel/dist/assets/owl.theme.default.min.css');
		wp_enqueue_style("slick-css",$this->stylesheet_dir_uri.'/assets/plugins/slick-master/slick.css');
		wp_enqueue_style("owltheme-css",$this->stylesheet_dir_uri.'/assets/plugins/slick-master/slick-theme.css');

		wp_enqueue_style('custom-css',$this->stylesheet_dir_uri.'/assets/css/style.css');
		wp_enqueue_style('style-css',$this->stylesheet_dir_uri.'/style.css');
		wp_enqueue_style('responsive-css',$this->stylesheet_dir_uri.'/assets/css/responsive.css');


		//load script
		wp_deregister_script('jquery');
		wp_enqueue_script( 'jqueryslim',$this->stylesheet_dir_uri.'/assets/plugins/jquery/jquery.slim.min.js',[], '321', true);
		wp_enqueue_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js',[],true);
		wp_enqueue_script( 'jquery');
		wp_enqueue_script( 'popper-js',$this->stylesheet_dir_uri.'/assets/plugins/bootstrap/js/popper.min.js',[], '', true);
		wp_enqueue_script( 'bootstrap-js',$this->stylesheet_dir_uri.'/assets/plugins/bootstrap/js/bootstrap.min.js',[], '', true);
		wp_enqueue_script( 'owljs',$this->stylesheet_dir_uri.'/assets/plugins/OwlCarousel/dist/owl.carousel.min.js',[], '', true);
		wp_enqueue_script( 'slik',$this->stylesheet_dir_uri.'/assets/plugins/slick-master/slick.min.js',[], '', true);
		wp_enqueue_script('wow-js',$this->stylesheet_dir_uri.'/assets/js/wow.min.js',[],'',true);
		wp_enqueue_script( 'main-js',$this->stylesheet_dir_uri.'/assets/js/init.js',[], '', true);
	}

	/**
	 * wordpress theme support setting
	 * @return null 
	*/
	public function theme_support(){
		add_theme_support('title-tag');
		add_theme_support( 'post-thumbnails');
		add_theme_support('custom-logo',[
			'header-text' => ['site-title','site-description'],
			'height'=>85,
			'width'=>85,
			'flex-height'=>true,
			'flex-width'=>true,
		]);
	}

	/**
	 * register menu type
	 * @return null 
	*/
	public function register_menus(){
		register_nav_menus([
			'primary-menu' => esc_html__('Primary Menu','rough ridge'),
			'footer-menu' => esc_html__('Footer Menu')
		]);
	}

	/**
	 * accept line break by text editor
	**/
	public function clear_br($content){ 
		return str_replace("<br/>","<br clear='none'/>", $content);
	} 

}

Extras::get_instance();
