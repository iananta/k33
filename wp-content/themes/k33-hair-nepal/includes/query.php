<?php 
	class Query{
		/**
		 * @return services query 
		 */
		public static function services($post_per_page=null){
			$args=[
				'post_type'=>'services',
				'post_status'=>'publish',
		    	'posts_per_page'=>$post_per_page ?? -1,
			];
			return new Wp_Query($args);
		}
		/**
		 * @return testimonial
		 */
		public static function testimonials(){
			$args=[
				'post_type'=>'testimonial',
				'post_status'=>'publish',
		    	'posts_per_page'=>-1,
			];
			return new Wp_Query($args);
		}

		/**
		 * @return blog list
		 */
		public static function blogs($post_per_page){
			$args=[
				'post_type'=>'blogs',
				'post_status'=>'publish',
				'posts_per_page'=>$post_per_page,
				'order'=>'desc'
			];
			return new Wp_Query($args);
		}

		/**
		 * @return new hair unit list
		 */
		public static function getNewHairUnit(){
			$args=[
				'post_type'=>'hair_units',
				'post_status'=>'publish',
				'post_per_page'=>-1,
				'meta_query' => array(
					array(
					    'key'     => 'hair_unit_type',
					    'value'   => 'new_hair_unit',
					    'compare' => '=',
				    ),
				 )
			];
			return new Wp_Query($args);
		}

		/**
		 * @return maintenance hair list
		 */
		public static function getMaintenanceHaitUnit(){
			$args=[
				'post_type'=>'hair_units',
				'post_status'=>'publish',
				'post_per_page'=>-1,
				'meta_query' => array(
					array(
					    'key'     => 'hair_unit_type',
					    'value'   => 'maintenance_hair_unit ',
					    'compare' => '=',
				    ),
				 )
			];
			return new Wp_Query($args);
		}
	}
?>