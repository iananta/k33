<?php
/** branch setting **/
Kirki::add_panel( 'setting_panel', array(
    'priority'    => 160,
    'title'       => esc_html__( 'Setting Panel', 'kirki' ),
) );

/*Kirki::add_section( 'branch_setting', array(
	'title'          => esc_html__( 'Branch Setting', 'kirki' ),
	'panel'=> 'setting_panel',
	'priority'       => 10,
) );

Kirki::add_field( 'branchs', [
	'type'        => 'repeater',
	'label'       => esc_html__( 'Branchs', 'kirki' ),
	'section'     => 'branch_setting',
	'priority'    => 10,
	'row_label' => [
		'type'  => 'field',
		'value' => esc_html__( 'Branch Item', 'kirki' ),
	],
	'button_label' => esc_html__('"Add new"', 'kirki' ),
	'settings'     => 'branchs',
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'branchs' => array(
			'selector' => '.edit-branch',
			'render_callback' => '__return_false'
		)
	),
	'fields' => [
		'location'=>[
			'type'        => 'text',
			'label'       => esc_html__( 'Location', 'kirki' ),
		],
		'address'=>[
			'type' =>'text',
			'label' => esc_html('Address','kirki'),
		],
		'fax'=>[
			'type'=>'text',
			'label' => esc_html('Fax','kirki'),
		],
		'phone'=>[
			'type'=>'text',
			'label' => esc_html('Phone No','kirki'),
		],
		'email'=>[
			'type'=>'text',
			'label' => esc_html('Email','kirki'),
		],

	],
	'choices' => [
		'limit' => 3
	],
	
] );*/
Kirki::add_section( 'contact_setting', array(
	'title'          => esc_html__( 'Contact Setting', 'kirki' ),
	'panel'=> 'setting_panel',
	'priority'       => 10,
) );
Kirki::add_field( 'phone', [
	'settings' => 'phone',
	'label'    => __( 'Phone No', 'kirki' ),
	'section'  => 'contact_setting',
	'type'     => 'text',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'phone' => array(
			'selector' => '.edit-contact-phone',
			'render_callback' => '__return_false'
		)
	)
] );
Kirki::add_field( 'email', [
	'settings' => 'email',
	'label'    => __( 'Email Address', 'kirki' ),
	'section'  => 'contact_setting',
	'type'     => 'text',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'email-contact' => array(
			'selector' => '.edit-contact-email',
			'render_callback' => '__return_false'
		)
	)
] );
Kirki::add_field( 'location', [
	'settings' => 'location',
	'label'    => __( 'Location', 'kirki' ),
	'section'  => 'contact_setting',
	'type'     => 'text',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'location-contact' => array(
			'selector' => '.edit-contact-location',
			'render_callback' => '__return_false'
		)
	)
] );
Kirki::add_section( 'social_setting', array(
	'title'          => esc_html__( 'Social Setting', 'kirki' ),
	'panel'=> 'setting_panel',
	'priority'       => 10,
) );
Kirki::add_field( 'facebook_link', [
	'settings' => 'facebook_link',
	'label'    => __( 'Facebook Link', 'kirki' ),
	'section'  => 'social_setting',
	'type'     => 'url',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'facebook_link' => array(
			'selector' => '.edit-facebook',
			'render_callback' => '__return_false'
		)
	)
] );
Kirki::add_field( 'youtube_link', [
	'settings' => 'youtube_link',
	'label'    => __( 'YouTube Link', 'kirki' ),
	'section'  => 'social_setting',
	'type'     => 'url',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'youtube_link' => array(
			'selector' => '.edit-youtube',
			'render_callback' => '__return_false'
		)
	)
] );
Kirki::add_field( 'instagram_link', [
	'settings' => 'instagram_link',
	'label'    => __( 'Instagram Link', 'kirki' ),
	'section'  => 'social_setting',
	'type'     => 'url',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'instagram_link' => array(
			'selector' => '.edit-instagram',
			'render_callback' => '__return_false'
		)
	)
] );

/** end setting **/
/** destination customizer **/
Kirki::add_panel( 'destination_panel', array(
    'priority'    => 170,
    'title'       => esc_html__( 'Destination Panel', 'kirki' ),
) );
Kirki::add_section( 'destination_section', array(
	'title'          => esc_html__( 'Destination Section', 'kirki' ),
	'panel'=> 'destination_panel',
	'priority'       => 10,
) );
Kirki::add_field( 'destinations', [
	'type'        => 'repeater',
	'label'       => esc_html__( 'Destinations', 'kirki' ),
	'section'     => 'destination_section',
	'priority'    => 10,
	'row_label' => [
		'type'  => 'field',
		'value' => esc_html__( 'Destination Item', 'kirki' ),
	],
	'button_label' => esc_html__('"Add new"', 'kirki' ),
	'settings'     => 'destinations',
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'destinations' => array(
			'selector' => '.edit-destination',
			'render_callback' => '__return_false'
		)
	),
	'fields' => [
		'destination_logo'=>[
			'type'        => 'image',
			'label'       => esc_html__( 'Image', 'kirki' ),
		],
		'destination_url'=>[
			'type' =>'url',
			'label' =>esc_html__( 'Destination Url', 'kirki' ),
		],
		'description'=>[
			'type'        => 'textarea',
			'label'       => esc_html__( 'Description', 'kirki' ),
		],
	],
	'choices' => [
		'limit' => 2
	],
	
] );
Kirki::add_field( 'destination', [ ]);

/** end of destination customizer **/
/** banner customizer **/
Kirki::add_panel( 'banner_panel', array(
    'priority'    => 180,
    'title'       => esc_html__( 'Banner Panel', 'kirki' ),
) );
Kirki::add_section( 'banner_section', array(
	'title'          => esc_html__( 'Banner Section', 'kirki' ),
	'panel'=> 'banner_panel',
	'priority'       => 10,
) );
Kirki::add_field( 'banners', [
	'type'        => 'repeater',
	'label'       => esc_html__( 'Banners', 'kirki' ),
	'section'     => 'banner_section',
	'priority'    => 10,
	'row_label' => [
		'type'  => 'field',
		'value' => esc_html__( 'Banner Item', 'kirki' ),
	],
	'button_label' => esc_html__('"Add new"', 'kirki' ),
	'settings'     => 'banners',
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'banners' => array(
			'selector' => '.edit-banner',
			'render_callback' => '__return_false'
		)
	),
	'fields' => [
		'banner_image'=>[
			'type'        => 'image',
			'label'       => esc_html__( 'Image', 'kirki' ),
		],
	],
	
] );


Kirki::add_section( 'inner_banner_section', array(
	'title'          => esc_html__( 'Inner Page Banner Section', 'kirki' ),
	'panel'=> 'banner_panel',
	'priority'       => 20,
) );
Kirki::add_field( 'inner_page_banner_image', [
	'settings' => 'inner_page_banner_image',
	'label'    => __( 'Inner Page Banner Image', 'kirki' ),
	'section'  => 'inner_banner_section',
	'type'     => 'image',
	'priority' => 10,
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'email' => array(
			'selector' => '.edit-inner-page-banner-image',
			'render_callback' => '__return_false'
		)
	)
] );

/** end of banner **/

/** gallery customizer **/
Kirki::add_panel( 'gallery_panel', array(
    'priority'    => 190,
    'title'       => esc_html__( 'Gallery Panel', 'kirki' ),
) );
Kirki::add_section( 'gallery_section', array(
	'title'          => esc_html__( 'Gallery Section', 'kirki' ),
	'panel'=> 'gallery_panel',
	'priority'       => 10,
) );
Kirki::add_field( 'galleries', [
	'type'        => 'repeater',
	'label'       => esc_html__( 'Galleries', 'kirki' ),
	'section'     => 'gallery_section',
	'priority'    => 10,
	'row_label' => [
		'type'  => 'field',
		'value' => esc_html__( 'Gallery Item', 'kirki' ),
	],
	'button_label' => esc_html__('"Add new"', 'kirki' ),
	'settings'     => 'galleries',
	'transport' => 'postMessage',    
	'partial_refresh' => array(
		'galleries' => array(
			'selector' => '.edit-gallery',
			'render_callback' => '__return_false'
		)
	),
	'fields' => [
		'gallery_image'=>[
			'type'        => 'image',
			'label'       => esc_html__( 'Image', 'kirki' ),
		],
	],
	
] );
/** end of gallery customizer **/