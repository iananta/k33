<?php

class CustompostType{
	public static $instance=null;
	public function __construct(){
		add_action('init',[$this,'register_customposttype']);
		//add_action('init',[$this,'register_custom_taxonomies']);
	}
	public static function get_instance() {
		if(!self::$instance || self::$instance == null) {
			self::$instance = new self();
		}
		return self::$instance;
	} 
	public function register_customposttype(){
		$post_types=[
			'blogs'=>[
				'name'=>'Blogs',
				'singular_name'=>'Blog',
				'publicly_queryable' => true,
				'hierarchical'=> true,
				'menu_icon'=>'dashicons-admin-page',
				'supports'=>['title','thumbnail','editor']
			],
			'services'=>[
				'name'=>'Services',
				'singular_name'=>'Service',
				'publicly_queryable' => true,
				'hierarchical'=> true,
				'menu_icon'=>'dashicons-admin-page',
				'supports'=>['title','thumbnail','editor']
			],
			'testimonial'=>[
				'name'=>'Testimonials',
				'singular_name'=>'Testimonial',
				'publicly_queryable' => false,
				'hierarchical'=> false,
				'menu_icon'=>'dashicons-admin-comments',
				'supports'=>['title','editor','thumbnail']
			],
			'hair_units'=>[
				'name'=>'Hait Units',
				'singular_name'=>'Hair Unit',
				'publicly_queryable' => false,
				'hierarchical'=> false,
				'menu_icon'=>'dashicons-admin-comments',
				'supports'=>['title']
			],
			
			
			
		];
		foreach($post_types as $key=>$post_type)
		{

			$labels = array(
				'name' => _x($post_type['name'], 'post type general name'),
				'singular_name' => _x($post_type['singular_name'], 'post type singular name'),
				'add_new' => _x('Add New', ''),
				'add_new_item' => __('Add New '.$post_type['singular_name']),
				'edit_item' => __('Edit '.$post_type['singular_name']),
				'new_item' => __('New '.$post_type['singular_name']),
				'view_item' => __('View '.$post_type['singular_name']),
				'search_items' => __('Search '.$post_type['singular_name']),
				'not_found' =>  __('Not Found'),
				'not_found_in_trash' => __('Not found in Trash'),
				'menu_name' => $post_type['name']
			);
			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => $post_type['publicly_queryable'],
				'show_ui' => true,
				'show_in_menu' => true,
				'query_var' => true,
				'rewrite' => true,
				'capability_type' => 'post',
				'has_archive' => false,
				'hierarchical' =>  $post_type['hierarchical'],
				'menu_position' =>$post_type['menu_position'] ?? null,
				'menu_icon' => $post_type['menu_icon'],
				'supports' => $post_type['supports'] ?? array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions' ),
				'rewrite' => array(
					'with_front' => FALSE,
				)
			);
			register_post_type($key,$args);
		}
		flush_rewrite_rules();
	}
	public function register_custom_taxonomies(){
		foreach($taxonimies as $key => $tax):
			$labels = array(
				'name' => _x( $tax['plural_name'], 'taxonomy general name' ),
				'singular_name' => _x( $tax['singular_name'], 'taxonomy singular name' ),
				'search_items' =>  __( 'Search Types' ),
				'all_items' => __( 'All '.$tax['singular_name'] ),
				'parent_item'       => __( 'Parent Category' ),
				'parent_item_colon' => __( 'Parent Category:' ),
				'edit_item' => __( 'Edit '.$tax['singular_name'] ),
				'update_item' => __( 'Update '.$tax['singular_name'] ),
				'add_new_item' => __( 'Add New '.$tax['singular_name'] ),
				'new_item_name' => __( 'New '.$tax['singular_name'].' Name' ),
			);


			register_taxonomy($key,$tax['post_type'], array(
				'hierarchical' => $tax['hierarchical'] ?? false,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => $tax['slug'] ),
			));
		endforeach;
	}
}

Customposttype::get_instance();
