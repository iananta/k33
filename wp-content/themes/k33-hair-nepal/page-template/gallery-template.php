<?php
/**
* Template Name:K33 Hair Nepal Gallery Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
$galleries=Helperfunction::getGalleries();
get_header('main');
get_template_part('template-parts/partial/inner-page-banner');
?>

<div class="inner-galler-wrapper">
	<div class="container">
		<div class="gallery-items">
			<?php foreach($galleries as $gallery):
				$image=wp_get_attachment_image_src($gallery['gallery_image'], 'full' );
			 ?>
			<div class="gallery-item gallery-pop-item">
				<div class="gallery-image">
					<a href="<?php echo $image[0]; ?>"><img class="wow zoomIn" src="<?php echo $image[0]; ?>"></a>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<?php
	get_footer('main');
?>