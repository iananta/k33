<?php
/**
* Template Name:K33 Hair Nepal Home Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
$aboutUs=Helperfunction::getAboutus();
$galleries=Helperfunction::getGalleries();
get_header('main');
get_template_part('template-parts/partial/banner');
?>

<!-- about us section -->
	<section class="about-us">
		<div class="container">
			<div class="row about-us-center">
				<div class="col-md-6 wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.3s">
					<div class="about-us-text">
						<div class="section-title">
							<h2><?php echo $aboutUs->post_title; ?></h2>
						</div>
						<p>
							<?php 
								$aboutContent=apply_filters( 'the_content', $aboutUs->post_content );
								echo substr($aboutContent,0,503);
							?>
						</p>
						<div class="section-button">
							<a href="<?php echo get_permalink($aboutUs->ID); ?>">READ MORE</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 wow slideInRight" data-wow-duration="2s" data-wow-delay="0.3s">
					<div class="about-us-img">
						<div class="img-content">
							<?php
								$aboutImage=wp_get_attachment_image_src( get_post_thumbnail_id( $aboutUs->ID ), 'full' );
							 ?>
							<img src="<?php echo $aboutImage[0]; ?>" alt="">
						</div>
						<div class="b-c"></div>
					</div>

				</div>
			</div>
		</div>
	</section>
<!-- end of about section -->
<!-- online enquiry and services section -->
	<section class="enquiry-services">
		<div class="container">
			<div class="row">
				<div class="col-md-6 enquiry-services-items wow slideInLeft">
					<!-- online enquiry form -->
					<div class="form-wrapper">
						<div class="enquiry-form-wrapper">
							<div class="form-title">
								<h1>Online Enquiry form</h1>
							</div>
							<?php echo do_shortcode('[contact-form-7 id="36" title="Retail Store Enquiry Form" html_class="enquiry-form"]'); ?>
						</div>
					</div>
					<!-- end of online enquiry form -->
				</div>
				<div class="col-md-6 enquiry-services-items wow slideInRight">
					<div class="section-title">
						<h2>Services</h2>
					</div>	
					<!-- ----------slick vertical slider---------- -->
					<div class="service-slider-wrapper">
						<!-- for custom arrow buttons -->
						<button class="service-slider-btn prev-button">
							<i class="fa fa-long-arrow-up"></i>
						</button>
						<button class="service-slider-btn next-button">
							<i class="fa fa-long-arrow-down"></i>
						</button>
						<div class="service-vertical-slider slider">
							<?php 
							  $services=Query::services();
							  if($services->have_posts()):
							  	while($services->have_posts()):
							  		$services->the_post();
							  		$title=get_the_title();
							?>
							<a href="#"><?php echo $title; ?></a>

							<?php 
								 endwhile;
								endif; 
							?>
						</div>
					</div>
					<!-- end of slick vertical slider -->
				</div>
			</div>
		</div>
	</section>
<!-- end of online enquiry and services section -->


<?php 
get_template_part('template-parts/partial/blog');
get_template_part('template-parts/partial/testimonial');
get_footer('main');
?>
