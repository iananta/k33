<?php
/**
* Template Name:K33 Hair Nepal Hair Units Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
get_header();
$newHairs=Query::getNewHairUnit();
$maintenances=Query::getMaintenanceHaitUnit();
$count=0;
$user=wp_get_current_user();
?>
<header class="dashboard-header">
	<div class="table-container">
		<div class="header-wrapper">
			<div class="user_name">
				<h1><i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo $user->display_name; ?></h1>
			</div>
			<div class="header-action-button">
				<p><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<a href="<?php echo wp_logout_url( get_permalink(60) ); ?>">Logout</a></p>
			</div>
		</div>
		
	</div>
</header>
<section class="table_section">
	<div class="table-container">
		<div class="table_wrapper">
			<div class="table-title">
				<h1>New Hair Unit</h1>
			</div>
			<table class="unit-table" id="new-hair">
				<thead>
					<tr class="main-table-head">
						<th colspan="7">Order Details</th>
						<th colspan="13">Production Status</th>
					</tr>
					<tr class="secondary-table-head">
						<th>
							S.no
						</th>
						<th>
							Box No
						</th>
						<th>Order Type</th>
						<th>Hair Hotel Order Date</th>
						<th>Factory Order Received Date</th>
						<th>Customer Name</th>
						<th>Customer Id</th>

						<th>Production Dept</th>
						<th>Base Making Dept</th>
						<th>Blending Dept</th>
						<th>Knotting Dept</th>
						<th>Knotting Quality Check</th>
						<th>After Knotting Dept</th>
						<th>Dyeing Dept</th>
						<th>Curling Dept</th>
						<th>Finishing Dept</th>
						<th>Final quality check</th>
						<th>Finished Date</th>
						<th>Shipout Date</th>
						<th>Remark</th>
					</tr>
				</thead>
				<tbody>
				<?php while($newHairs->have_posts()):
						$newHairs->the_post();
						$id=get_the_ID();
						$boxNo=get_field('box_no',$id,true);
						$orderType=get_field('order_type',$id,true);
						$hairHoteOrderDate=get_field('hair_hotel_order_date',$id,true);
						$factoryReceivedDate=get_field('factory_order_received_date',$id,true);
						$customerName=get_field('customer_name',$id,true);
						$customerId=get_field('customer_id',$id,true);
						$productionDepartment=get_field('production_department',$id,true);
						$baseMakeDepartment=get_field('base_making_department',$id,true);
						$blendingDepartment=get_field('blending_department',$id,true);
						$knottingDepartment=get_field('knotting_department',$id,true);
						$knottingQualityCheck=get_field('knotting_quality_check',$id,true);
						$afterKnottingDepartment=get_field('after_knotting_department',$id,true);
						$dyeingDepartment=get_field('dyeing_department',$id,true);
						$curlingDepartment=get_field('curling_department',$id,true);
						$finishingDepartment=get_field('finishing_department',$id,true);
						$finalQualityCheck=get_field("final_quality_check",$id,true);
						$finishedDate=get_field('finished_date',$id,true);
						$shipoutDate=get_field('shipout_date',$id,true);
						$remark=get_field('remark',$id,true);


				 ?>
					<tr>
						<td><?php echo $count + 1 ?></td>
						<td><?php echo $boxNo; ?></td>
						<td><?php echo $orderType; ?></td>
						<td><?php echo $hairHoteOrderDate; ?></td>
						<td><?php echo $factoryReceivedDate; ?></td>
						<td><?php echo $customerName; ?></td>
						<td><?php echo $customerId; ?></td>
						<td><?php echo $productionDepartment; ?></td>
						<td><?php echo $baseMakeDepartment; ?></td>
						<td><?php echo $blendingDepartment; ?></td>
						<td><?php echo $knottingDepartment; ?></td>
						<td><?php echo $knottingQualityCheck; ?></td>
						<td><?php echo $afterKnottingDepartment; ?></td>
						<td><?php echo $dyeingDepartment; ?></td>
						<td><?php echo $curlingDepartment?></td>
						<td><?php echo $finishingDepartment ?></td>
						<td><?php echo $finalQualityCheck; ?></td>
						<td><?php echo $finishedDate; ?></td>
						<td><?php echo $shipoutDate ?></td>
						<td><?php echo $remark; ?></td>
					</tr>
				  <?php endwhile; ?>
				</tbody>
			</table>
		</div>
		<div class="table_wrapper">
			<div class="table-title">
				<h1>Maintenance Hair Unit</h1>
			</div>
			<table class="unit-table" id="maintenance-hair">
				<thead>
					<tr class="main-table-head">
						<th colspan="7">Order Details</th>
						<th colspan="13">Maintenance Status</th>
					</tr>
					<tr class="secondary-table-head">
						<th>
							S.no
						</th>
						<th>
							Box No
						</th>
						<th>Hair Hotel Received Date</th>
						<th>Factory Order Received Date</th>
						<th>Customer Name</th>
						<th>Customer Id</th>

						<th>Maintenance Dept</th>
						<th>Clearing Dept</th>
						<th>Net Repair</th>
						<th>Blending Dept</th>
						<th>Knotting Dept</th>
						<th>Knotting Quality Check</th>
						<th>Dyeing Dept</th>
						<th>Curling Dept</th>
						<th>Finishing Dept</th>
						<th>Final Quality check</th>
						<th>Finished Date</th>
						<th>Shipout Date</th>
						<th>Remark </th>
					</tr>
				</thead>
				<tbody>
					<?php while($maintenances->have_posts()):
						$maintenances->the_post();
						$boxNo=get_field('box_no',$id,true);
						$hairHotelReceivedDate=get_field('hair_hotel_received_date',$id,true);
						$factoryReceivedDate=get_field('factory_order_received_date',$id,true);
						$customerName=get_field('customer_name',$id,true);
						$customerId=get_field('customer_id',$id,true);

						$maintenanceDepartment=get_field('maintenance_department',$id,true);
						$clearingDepartment=get_field('clearing_department',$id,true);
						$netRepair=get_field('net_repair',$id,true);
						$blendingDepartment=get_field('blending_department',$id,true);
						$knottingDepartment=get_field('knotting_department',$id,true);
						$knottingQualityCheck=get_field('knotting_quality_check',$id,true);
						$dyeingDepartment=get_field('dyeing_department',$id,true);
						$curlingDepartment=get_field('curling_department',$id,true);
						$finishingDepartment=get_field('finishing_department',$id,true);
						$finalQualityCheck=get_field("final_quality_check",$id,true);
						$finishedDate=get_field('finished_date',$id,true);
						$shipoutDate=get_field('shipout_date',$id,true);
						$remark=get_field('remark',$id,true);

					 ?>
					<tr>
						<td><?php echo $count +1; ?></td>
						<td><?php echo $boxNo; ?></td>
						<td><?php echo $hairHotelReceivedDate;?></td>
						<td><?php echo $factoryReceivedDate; ?></td>
						<td><?php echo $customerName; ?></td>
						<td><?php echo $customerId; ?></td>
						<td><?php echo $maintenanceDepartment; ?></td>
						<td><?php echo $clearingDepartment; ?></td>
						<td><?php echo $netRepair; ?></td>
						<td><?php echo $blendingDepartment; ?></td>
						<td><?php echo $knottingDepartment; ?></td>
						<td><?php echo $knottingQualityCheck; ?></td>
						<td><?php echo $dyeingDepartment; ?></td>
						<td><?php echo $curlingDepartment?></td>
						<td><?php echo $finishingDepartment ?></td>
						<td><?php echo $finalQualityCheck; ?></td>
						<td><?php echo $finishedDate; ?></td>
						<td><?php echo $shipoutDate ?></td>
						<td><?php echo $remark; ?></td>
					</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
<?php
get_footer();

?>