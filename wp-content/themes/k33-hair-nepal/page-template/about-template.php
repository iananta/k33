<?php
/**
* Template Name:K33 Hair Nepal About Us Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
$aboutUs=Helperfunction::getAboutus();
get_header('main');
get_template_part('template-parts/partial/inner-page-banner');
?>
<!-- about us section -->
	<section class="about-us">
		<div class="container">
			<div class="row about-us-center">
				<div class="col-md-6">
					<div class="about-us-text">
						<div class="section-title">
							<h2><?php echo $aboutUs->post_title; ?></h2>
						</div>
						<p>
							<?php 
								$aboutContent=apply_filters( 'the_content', $aboutUs->post_content );
								echo $aboutContent;
							?>
						</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="about-us-img">
						<div class="img-content">
							<?php
								$aboutImage=wp_get_attachment_image_src( get_post_thumbnail_id( $aboutUs->ID ), 'full' );
							 ?>
							<img src="<?php echo $aboutImage[0]; ?>" alt="">
						</div>
						<div class="b-c"></div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- end of about section -->


<?php 
get_template_part('template-parts/partial/testimonial');
get_footer('main');

?>