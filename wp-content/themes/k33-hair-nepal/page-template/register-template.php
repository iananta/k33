<?php

/**
* Template Name:K33 Hair Nepal Register Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/auth.css">
	<?php wp_head(); ?>
</head>
<body>
	<div class="authentication_wrapper">

		<div class="form_wrapper auth_wrapper register_wrapper">
			  <form id="register_form" method="POST">
    
			    <p>
			    	<label>First Name</label>
			    	<input type="text" name="register_meta[first_name]"  value="<?php echo isset($_POST["first_name"]) ? $_POST["first_name"] : ''; ?>"required >
			    </p>
			     <p>
			     	<label>Last Name</label>
			     	<input type="text" name="register_meta[last_name]"  value="<?php echo isset($_POST["last_name"]) ? $_POST["last_name"] : ''; ?>" required ></p>
			     <p>
			     	<label>Username</label>
			     	<input type="text" name="register_meta[user_login]" value="<?php echo isset($_POST["user_login"]) ? $_POST["user_login"] : ''; ?>"required >
			     </p>
			    <p>
			    	<label>Email</label>
			    	<input type="email" name="register_meta[user_email]" value="<?php echo isset($_POST["user_email"]) ? $_POST["user_email"] : ''; ?>"required >
			    </p>
			   
			    <p>
			    	<label>Password</label>
			    	<input type="password" name="register_meta[password]"  value="<?php echo isset($_POST["password"]) ? $_POST["password"] : '';  ?>"required >
			    </p>
			    <p>
			    	<label>Confirm Password</label>
			    	<input type="password" name="register_meta[confirm_password]"  value="<?php echo isset($_POST["confirm_password"]) ? $_POST["confirm_password"] : '';  ?>" required >
			    </p>
			    <input type="hidden" name="action" value="registration_process">
			    <?php wp_nonce_field( 'form_nonce_registration', 'form_nonce_registration' ); ?>
			   
			     <button type="submit" id="wp-submit" >Register<div id="loader" style="display: none"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></div></button>
			     
 			</form>
		
			<div class="register_button">
				<a href="<?php echo get_permalink(60); ?>">Login</a>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
			$('#register_form').on('submit', function(e) {
			e.preventDefault();
			let _this = $(this);
			$.ajax({
				url:ajax_url,
				type:'POST',
				data:_this.serialize(),
				beforeSend: function() {
					let btn = _this.find('input[type="submit"],input[type="button"],a');
					btn.attr('disabled', 'disabled');
					_this.find('#loader').show();
					_this.find('.alert-message').remove();					
				},
				success:function(data){
					_this.find('#loader').hide();
					_this.find('input[type="submit"],input[type="button"],a').removeAttr('disabled');
					if(data && data.type) {
						if(data.type=='error'){
							_this.prepend('<div class="alert-message '+data.type+'">'+data.message+'</div>')
						}
						if(data.type=='success'){
							swal(
								'Success',
								data.message,
								data.type
								)
							_this[0].reset();
						}
						
					}
				}
			});
		});
	})
</script>
</html>