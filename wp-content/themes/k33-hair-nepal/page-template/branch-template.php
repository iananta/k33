<?php

/**
* Template Name:K33 Hair Nepal Branch Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
get_header('main');
get_template_part('template-parts/partial/inner-page-banner');

?>
<section id="inner-section" class="branch-section">
	<div class="container">
		<div class="branch-details-wrapper">
			<div class="branch-image">
				
			</div>
			<div class="branch-details">
				
			</div>
		</div>
	</div>
</section>

<?php
get_footer('main') 
?>