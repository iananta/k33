<?php

/**
* Template Name:K33 Hair Nepal Login Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/auth.css">
	<?php wp_head(); ?>
</head>
<body>
	<div class="authentication_wrapper">

		<div class="form_wrapper auth_wrapper">
			
		<?php

			if(!empty($_GET['login']) && $_GET['login'] == 'failed') {
				echo '<div class="alert error"><span style="font-size:14px;color:red">'.__( 'Invalid username or password', 'divi').'</span></div><br>';
			}
			$args = array(
				'echo'           => true,
				'remember'       => false,
				'redirect'       =>60,
				'form_id'        => 'loginform',
				'id_username'    => 'email',
				'id_password'    => 'pwd',
				'id_submit'      => 'wp-submit',
				'label_username' => __( 'Username' ),
				'label_password' => __( 'Passowrd' ),
				'label_log_in'   => __( 'Sign In' ),
				'value_username' => '',
				'value_remember' => false
			);
			wp_login_form($args);
		?>
			<!-- <div class="register_button">
				<a href="<?php echo get_permalink(122); ?>">Register</a>
			</div>
		</div> -->
	</div>
</body>
</html>