<?php
/**
* Template Name:K33 Hair Nepal Contact Page
*
* @package WordPress
* @subpackage 
* @since LK 1.0
*/
$branchsId=Helperfunction::getBranchsPageId();
$branchs=[
	 	0=> Helperfunction::getBranch($branchsId['chabahil']),
	    1=>Helperfunction::getBranch($branchsId['koteshwor']),
	    2=>Helperfunction::getBranch($branchsId['tripureshowor']),
	 ];
get_header('main');
get_template_part('template-parts/partial/inner-page-banner');
?>
	<section class="enquiry-services">
		<div class="container">
			<div class="row">
				<div class="col-md-7 enquiry-services-items wow slideInLeft">
					<!-- online enquiry form -->
					<div class="form-wrapper">
						<div class="enquiry-form-wrapper">
							<?php echo do_shortcode('[contact-form-7 id="36" title="Retail Store Enquiry Form" html_class="enquiry-form contact-form"]'); ?>

						</div>
					</div>
					<!-- end of online enquiry form -->
				</div>
				<div class=" col-md-5 branch-info">
					<div class="branch-wrapper">
						<div class='branch-items'>
						<?php
							foreach($branchs as $branch):
								$id=$branch->ID;
								$location=$branch->post_title;
								$address=get_field('location',$id,true);
								$phone=get_field('phone_no',$id,true);
								$fax=get_field('fax',$id,true);
								$email=get_field('email',$id,true);
						?>
							<div class='branch-item'>
								<div class="branch_info_wraper">
									<div class="branch-title">
										<h1><?php echo $location; ?></h1>
										<div class="branch-other-info">
											<ul>
												<?php if($email): ?>
												<li><span class="branch-icon"><i class="fa fa-envelope"></i></span><span class="branch-info-content"><?php echo $email ?></span></li>
											    <?php endif; ?>
											    <?php if($phone): ?>
												<li><span class="branch-icon"><i class="fa fa-phone"></i></span><span class="branch-info-content"><?php  echo $phone; ?></span></li>
												<?php endif; ?>
												<?php if($fax): ?>
												<li><span class="branch-icon"><i class='fa fa-phone'></i></span><span class="branch-info-content"><?php echo $fax; ?></span></li>
											    <?php endif; ?>
												<li><span class="branch-icon"><i class="fa fa-map-marker"></i></span><span class="branch-info-content"><?php echo $address; ?></span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						<?php 
							endforeach;
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
	get_footer('main');
?>