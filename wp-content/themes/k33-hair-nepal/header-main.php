<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <meta http-equiv="refresh" content= "2"> -->
	<?php
	 	wp_head(); 
	 	$contactInfo=Helperfunction::getContactInfo();
	 	$social=Helperfunction::getSocialLinks();
	?>
</head>
<body>
	<!-- topbar -->
	<header class="topbar">
		<div class="container">
			<div class="topbar-content">
				<div class="topbar-social">
					<h2>Follow Us:</h2>
					<a class="edit-facebook" href="<?php echo $social['facebook']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/facebook_black.svg" alt=""></a>
					<a class="edit-instagram" href="<?php echo $social['instagram']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/instagram_black.svg" alt=""></a>
					<a class="edit-youtube" href="<?php echo $social['youtube']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/youtube_black.svg" alt=""></a>
				</div>
				<div class="topbar-contact">
					<a href="tel:<?php echo $contactInfo; ?>" class="edit-contact-phone">Hotline:&nbsp;&nbsp;<?php echo $contactInfo['phone']; ?></a>
					<span>|</span>
					<a href="mail:<?php $contactInfo['email']; ?>" class="edit-contact-email">Email:&nbsp;&nbsp;<?php echo $contactInfo['email']; ?></a>
					<span>|</span>
					<a href="#" class="edit-contact-location">Location:&nbsp;&nbsp;<?php echo $contactInfo['location']; ?></a>
				</div>
				<!-- <div class="topbar-location">
					<a href="#">Location <i class="fa fa-map-marker"></i></a>
				</div> -->
			</div>
		</div>
	</header>
		<!-- navigation bar -->
	<section class="navigation-bar">
		<div class="container">
			<!-- main navbar -->
			<nav class="main-nav navigation-contents">
				<div class="mobile-menu">
					<div class="main-logo">
						<a href="<?php echo get_permalink(10); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/k33_hair_logo.png" alt="company logo"></a>
					</div>
					<div class="navigation-toggle-btn">
						<div class="navigation-button navigation-trigger">
							<div class="burger"></div>
						</div>
					</div>
				</div>
				<div class="navigation-links">
					<?php
						$menu_location = 'primary-menu';
						wp_nav_menu(
							array(
								'theme_location' => $menu_location,
								'container'         => 'ul',
								'menu_class'    => 'main_navigation',
								'menu_id'        => ' ',
								'add_li_class'  => ' '
							)
						);
					?>

				</div>
				<div class="sub-logo">
					<a href="<?php echo get_site_url() ?>/login">Switch to</a>
					<a href="<?php echo get_site_url() ?>/login"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/k33_europe_logo.png" alt="second logo"></a>
				</div>
			</nav>
			<!-- end of main navbar -->
			<!-- mobile nav -->
			<nav class="mobile-nav">
		
				<div class="mobile-nav-content">
					<div class="left-content">
						<div class="main-logo">
							<a href="<?php echo get_permalink(10); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/k33_hair_logo.png" alt=""></a>
						</div>
						<a href="javascript:void(0);" onclick="openNav()">
							<div class="mobile-menu-button">
								<div class="menu-bars"></div>
							</div>
						</a>
					</div>
					<div id="mobile-nav-menu">
						<div class="right-content" id="mySidebar">
							<div class="mobile-nav-close">
								<a href="javascript:void(0);" onclick="closeNav()"><i class="fa fa-times"></i></a>

							</div>
							<div class="mobile-nav-links">
								<?php
								$menu_location = 'primary-menu';
								wp_nav_menu(
									array(
										'theme_location' => $menu_location,
										'container'         => 'ul',
										'menu_class'    => 'main_navigation',
										'menu_id'        => ' ',
										'add_li_class'  => ' '
									)
								);
								?>
							</div>
						</div>
					</div>
				</div>
				
			</nav>
			<!-- end of mobile navbar -->
		</div>
	</section>
	<!-- end of navigation bar -->