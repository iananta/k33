<?php 
get_header('main');
get_template_part('template-parts/partial/inner-page-banner');
$id=get_the_Id();
$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
$content=apply_filters('the_content',get_the_content());

?>

<div class="single-blog-section">
	<div class="container">
		<div class="single-blog-wrapper">
			<div class="single-blog-image">
				<?php if(has_post_thumbnail() ): ?>
				<img src="<?php echo $image[0]; ?>">
				<?php else: ?>
				<img src="<?php echo get_site_url(); ?>/wp-content/themes/k33-hair-nepal/assets/images/logo/k33_hair_logo.png">
				<?php endif; ?>
			</div>
			<div class="singel-blog-content">
				<div class="single-blog-heading">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
				<p><?php echo $content; ?></p>
			</div>
		</div>
	</div>
</div>

<?php 
	get_footer('main');
?>