  function openNav(){
         document.getElementById('mySidebar').style.width = "300px";
   }
   function closeNav() {
      document.getElementById('mySidebar').style.width = "0";
      document.getElementById('mobile-nav-menu').style.marginLeft = "0";
   }


function InitJs(){
    // banner slider owlCarousel
   this.bannerSliderInit = function(){
         $(".banner-slider-wrapper .owl-carousel").owlCarousel({
            loop: true,
            items: 1,
            smartSpeed: 450,
            // autoplay:true,
            // autoplayTimeout:5000,
            // autoplayHoverPause:true
          });
   },

   // service vertical slickSlider
   this.serviceVerticalSliderInit = function(){
      $(".service-vertical-slider").slick({
         autoplay: true,
        autoplaySpeed: 3000,
        infinite: false,
        speed: 3000,
        arrows: true,
        dots: false,
        vertical: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        // for costum arrows
        prevArrow:'.prev-button',
        nextArrow:'.next-button'
      });
   },

   // gallery owlCarousel Slider
   this.gallerySliderInit = function(){
      const nextIcon = '<i class="fa fa-long-arrow-right"></i>';
      const prevIcon = '<i class="fa fa-long-arrow-left"></i>';

      $(".gallery-slider-wrapper .owl-carousel").owlCarousel({
        loop: true,
        margin: 0,
        items: 6,
        nav: false,
        dots: false,
        slideBy: 2,
        responsive:{
        0:{
            items:2,
         },
        991:{
            items:4,
        },
        1400:{
            items:6

        }
      },
        navText: [
        prevIcon,
        nextIcon
        ]
      });
   },

   //testimonals slider owlCarousel
   this.testimonialSliderInit = function(){
      const nextIcon = '<i class="fa fa-long-arrow-right"></i>';
      const prevIcon = '<i class="fa fa-long-arrow-left"></i>';

      $("section.testimonials .owl-carousel").owlCarousel({
        loop: true,
        margin: 20,
        items: 3,
        nav: true,
        dots: false,
        responsive:{
        0:{
            items:1,
         },
        767:{
           items:2,
        },
        991:{
            items:3,
        }
      },
        navText: [
        prevIcon,
        nextIcon
        ]
      });
   },


   this.sliderInit = function(){
        this.bannerSliderInit();
        this.serviceVerticalSliderInit();
        this.gallerySliderInit();
        this.testimonialSliderInit();
   },
   
   this.galleryPopUpListener=function(){
        $(document).on('click','.gallery-pop-item',function(e){
            e.preventDefault()
            $(this).addClass('active')
            let image=$(this).find('a')
            $('.pop-image').attr('src',image.attr('href'))
            $('html').css('overflow','hidden')
            $('.gallery-pop-wrapper').css('display','flex')
        })
        function galleryProcess(target){
            target.addClass('active')
            let image=target.find('a')
            $('.pop-image').attr('src',image.attr('href'))
        }
        $(document).on('click','.next-gallery',function(){
            let findNext=$('.gallery-pop-item.active').next()
            console.log(findNext)
            let target
            if(findNext.length > 0){
                target=findNext
            }else{
                target=$('.gallery-pop-item:first')
            }
            $('.gallery-pop-item').removeClass('active')

            galleryProcess(target)

        })
        $(document).on('click','.prev-gallery',function(){
            let findPrev=$('.gallery-pop-item.active').prev()
            let target
            if(findPrev.length > 0){
                target=findPrev
            }else{
                target=$('.gallery-pop-item:last')
            }
            $('.gallery-pop-item').removeClass('active')

            galleryProcess(target)

        })
        $(document).on('click','.close-gallery ',function(){
            $('.gallery-pop-item').removeClass('active')
            $('html').removeAttr('style')

            $('.gallery-pop-wrapper').hide()
        })
  
    },
   this.init = function(){
      let _this = this;
      $(document).ready(function(){
         _this.sliderInit();
         _this.galleryPopUpListener()
      });
   }
}

let _obj = new InitJs();
    _obj.init();

