<?php 
get_header('main');
$post_name=get_post()->post_name ?? '';
get_template_part('template-parts/partial/inner-page-banner');

if(have_posts()):
			while ( have_posts() ) : 
				the_post();
				get_template_part('template-parts/content',$post_name);
			endwhile;
else:
	get_template_part('template-parts/content-none');
endif;
get_footer('main');