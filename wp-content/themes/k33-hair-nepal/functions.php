<?php
define('STYLESHEET_DIRECTORY',get_stylesheet_directory());
/**
 * extra settings
 * @return null
***/
require STYLESHEET_DIRECTORY.'/includes/extras.php';

/**
* custom post type
**/
require STYLESHEET_DIRECTORY.'/includes/customposttype.php';

/**
* query
**/

require STYLESHEET_DIRECTORY.'/includes/query.php';

/**
* helper function
**/
require STYLESHEET_DIRECTORY.'/includes/helperfunction.php';

/**
* customizer 
**/
require STYLESHEET_DIRECTORY.'/includes/customizer.php';


/**
** authetication
*/

require STYLESHEET_DIRECTORY.'/includes/auth.php';