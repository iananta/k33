

	<!-- bootstrap jquery -->
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

	<!-- bootstrap js -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/plugins/bootstrap/js/popper.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/plugins/dataTable/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			let newhair=$('#new-hair')
			let maitenance=$('#maintenance-hair')
			let options = {"sDom": 'rtip'}

			if(newhair){
				newhair.DataTable(options);
			}
			if(maitenance){
				maitenance.DataTable(options)
			}
		})
	</script>
</body>
</html>