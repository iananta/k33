<!DOCTYPE html>
<html lang="en" style="margin-top:0px !important">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/landing-page.css">
	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/plugins/dataTable/jquery.dataTables.min.css">

</head>
<?php 
	$destinations=Helperfunction::getDestinations();
?>
<body>
	<?php if(!is_page_template()): ?>
	<div class="container">
		
		<header>
			<a class="logo" href="#">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home_logo.png" alt="">
			</a>
			<!-- choosing section -->
			<section class="choice-section">
				<h1>CHOOSE YOUR DESTINATION</h1>
				<div class="choices">
					<?php foreach($destinations as $destination):
						$content=$destination['description'];
						$image=wp_get_attachment_image_src($destination['destination_logo'], 'full' );
						$url=$destination['destination_url']
					 ?>
					<div class="choice-box edit-destination">
						<a href="#"><img src="<?php echo $image[0]; ?>" alt=""></a>
						<p><?php echo $content; ?></p>
						<a href="<?php echo $url;  ?>" class="choice-button">CLICK HERE TO VISIT</a>
					</div>
					<?php endforeach;  ?>
				</div>
			</section>
			<!-- end of choosing section -->
		</header>
		<?php endif; ?>