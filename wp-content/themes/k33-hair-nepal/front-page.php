<?php
get_header();
 ?>
		<!-- join us section -->
		<section class="join-us">
			<div class="join-us-title">
				<h2>Enquiry</h2>
				<p>Please fill form below. Fields marked with <span>*</span> are required.</p>
			</div>
			<!-- join-us form -->
			<div class="form-wrapper">
				<?php echo do_shortcode('[contact-form-7 id="5" title="Enquiry Form" html_class="join-us-form"]'); ?>
			</div>
			<!-- end of join us form -->
		</section>
		<!-- end of join us section -->
	</div>


<?php get_footer(); ?>