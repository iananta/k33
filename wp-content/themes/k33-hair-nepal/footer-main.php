<?php 
	$branchsId=Helperfunction::getBranchsPageId();
	$destinations=Helperfunction::getDestinations();
	$galleries=Helperfunction::getGalleries();
	$social=Helperfunction::getSocialLinks();
	$branchs=[
	 	0=> Helperfunction::getBranch($branchsId['chabahil']),
	    1=>Helperfunction::getBranch($branchsId['koteshwor']),
	    2=>Helperfunction::getBranch($branchsId['tripureshowor']),
	 ];

?>	
	<!-- gallery slider section -->
	<section class="gallery">
			<div class="gallery-slider-wrapper">
				<div class="owl-carousel owl-theme">
					<?php 
						foreach($galleries as $gallery):
							$image=wp_get_attachment_image_src($gallery['gallery_image'], 'full' );
					 ?>
					<div class="items edit-gallery">
						<img class="wow zoomIn" src="<?php echo $image[0]; ?>" alt="">
					</div>
					<?php endforeach; ?>
				</div>
			</div>
	</section>
	<!-- gallery slider section end -->

	<!-- footer section -->
	<section class="footer">
		<div class="container">
			<div class="footer-logo">
				<?php 
				foreach($destinations as $destination):
					$image=wp_get_attachment_image_src($destination['destination_logo'], 'full' );
				?>
					<div class="footer-logo-1 edit-destination">
						<img class='wow zoomIn' src="<?php echo $image[0] ?>" alt="logo1">
					</div>
					<div class="footer-line">
						
					</div>
				<?php endforeach; ?>
			</div>

			<div class="footer-locations">
				<div class="row">
					<?php
						foreach($branchs as $branch):
							$id=$branch->ID;
							$location=$branch->post_title;
							$address=get_field('location',$id,true);
							$phone=get_field('phone_no',$id,true);
							$fax=get_field('fax',$id,true);
							$email=get_field('email',$id,true);
					?>
					<div class="col-md-4 wow zoomIn edit-branch">
						<div class="footer-location">
							<h2><a href="<?php echo get_permalink($id); ?>">K33 HAIR HOTEL (<?php echo $location; ?>)</a></h2>
							<div class="footer-location-underline-center">
								<div class="footer-location-underline"></div>
							</div>
							<p><?php echo $address ?></p>
							<?php if($phone): ?>
							<p><span>Phone no:</span><?php echo $phone; ?></p> 
							<?php endif; ?>
							<?php if($fax): ?>
							<p><span>Fax:</span><?php echo $fax; ?></p>
							<?php endif; ?>
							<?php if($email): ?>
							<p><span>Email:</span><?php echo $email; ?></p>
							<?php endif; ?>
						</div>
					</div>
					<?php
						endforeach;
					?>
				</div>
			</div>

			<div class="footer-social">
				<h2>Follow Us:</h2>
				<a class="edit-facebook" href="<?php echo $social['facebook']; ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/facebook_red.svg" alt="">
				</a>
				<a class="edit-instagram" href="<?php echo $social['instagram']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/instagram_red.svg" alt=""></a>
				<a class="edit-youtube" href="<?php echo $social['youtube']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/youtube_red.svg" alt=""></a>
			</div>

			<div class="footer-policy">
				<div class="footer-policy-terms">
					<a href="#">Privacy Policy.</a>
					<a href="#">Terms & Conditions</a>
				</div>
				<div class="footer-copyright">
					<p>@Copyright &copy; 2021. All rights reserved k33nepal</p>
				</div>
			</div>
		</div>
	</section>
	<div class="gallery-pop-wrapper">
		 <div class="close-gallery">
                    &times;
                </div>
            <div class="pop-image-wrapper">
                <div class="close-gallery">
                    &times;
                </div>
                <div class="prev-gallery">
                    <i class="fa fa-arrow-left"></i>
                </div>
                <div class="pop-wrap">
                    <img src="" class='pop-image'>
                </div>
                <div class="next-gallery">
                       <i class="fa fa-arrow-right"></i>

                </div>
            </div>
        </div>
    </div>
	<!-- end of footer section -->
	<?php wp_footer(); ?>
</body>
<script>
	new WOW().init();

	wow = new WOW(
	{
	boxClass:     'wow',      // default
	animateClass: 'animated', // default
	offset:       0,          // default
	mobile:       true,       // default
	live:         true        // default
	}
	)
wow.init();
</script>
</html>							