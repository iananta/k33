<?php
$testimonials=Query::testimonials();

?>	
	<!-- testimonials section -->
	<section class="testimonials">
		<div class="container">
			<div class="section-title">
				<h2>Testimonials</h2>
			</div>
			<div class="testimonials-slider-wrapper wow slideInRight" data-wow-duration="2s" data-wow-delay="0.3s">
				<div class="owl-carousel owl-theme">
					<?php while($testimonials->have_posts()):
						  $testimonials->the_post();
						  $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
					 ?>
					<div class="items">
						<div class="user-img">
							<img src="<?php  echo $image[0]; ?>" alt="">
						</div>
						<h2><?php echo get_the_title(); ?></h2>
						<p><?php echo get_the_content(); ?></p>
					</div>
					
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</section>
	<!-- end of testimonials section -->