<?php
	$banners=Helperfunction::getBanners();
?>
<!-- banner-slider -->
<section class="banner banner-slider-wrapper">
	<div class="owl-carousel owl-theme">
		<!-- banner-item 1 -->
		<?php 
			foreach($banners as $banner){ 
				  $image=wp_get_attachment_image_src($banner['banner_image'], 'full' );
		?>
			<div class="items edit-banner">
				<img src="<?php echo $image[0]; ?>" alt="">
			</div>
		<?php } ?>
	</div>
</section>
<!-- end of banner-slider -->
