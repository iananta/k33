<?php
	$post_per_page=is_page_template() ? 3 : -1;
	$blogs=Query::blogs($post_per_page);
?>


<!--blog section -->
	<section class="blog-section <?php echo is_page_template() ? 'home-blog' : '';  ?>" id="<?php echo is_page_template() ? '' : 'inner-section';  ?>">
		<div class="container">
			<?php  if(is_page_template()): ?>
			<div class="section-title">
				<h2>Blog</h2>
			</div>
			<?php endif; ?>
			<div class="blog-items-wrapper wow slideInLeft" data-wow-duration="2s" data-wow-delay="0.3s">
				<?php 
					while($blogs->have_posts()):
						$blogs->the_post();
						$id=get_the_ID();
						$title=get_the_title();
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
						$content=substr(get_the_content(),0,150);
						$permalink=get_the_permalink();
				?>
				<div class="blog-item">
					<div class="blog-item-wrapper">
						<div class="blog-image">
							<img src="<?php echo $image[0]; ?>">
						</div>
						<div class="blog-content">
							<h1><a href="<?php echo $permalink; ?>"><?php echo $title; ?></a></h1>
							<p><?php echo $content; ?></p>
						</div>
					</div>
				</div>
				<?php 
					endwhile;
				?>
			</div>
			<?php  if(is_page_template()): ?>
			<div class="section-button">
				<a href="<?php echo get_site_url() ?>/blogs">VIEW ALL</a>
			</div>
			<?php endif; ?>
		</div>
	</section>

<!-- end of blog section ->