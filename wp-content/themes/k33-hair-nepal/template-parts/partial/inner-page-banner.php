<?php 
	$innerBannerImage=Helperfunction::getInnerPageBanner();
?>
<div class="inner-page-banner-wrapper" style="background:url(<?php echo $innerBannerImage;  ?>);background-size:cover;width:100%;background-repeat: no-repeat;">
	<div class="container">
		<div class="inner-page_banner-content-wrapper edit-inner-page-banner-image">
			<div class="inner-title">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>