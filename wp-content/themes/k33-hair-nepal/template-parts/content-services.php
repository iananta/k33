<?php
	$services=Query::services();
?>
<div class="services-section" id="inner-section">
	<div class="container">
		<div class="services-items">
			<?php while($services->have_posts()):
					$services->the_post();
					$id=get_the_ID();
					$title=get_the_title();
					$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
			 ?>
			<div class='service-item'>
				<div class="service-image <?php echo has_post_thumbnail() ? '' : 'no-image' ?>">
					<?php if(has_post_thumbnail() ): ?>
					<img src="<?php echo $image[0]; ?>">
					<?php else:  ?>
						<img src="<?php echo get_site_url(); ?>/wp-content/themes/k33-hair-nepal/assets/images/logo/k33_hair_logo.png">
					<?php endif; ?>
				</div>
				<div class="secvice-contents">
					<h1><a href="<?php echo get_the_permalink(); ?>"><?php echo $title; ?></a></h1>
				</div>
			</div>
		   <?php  endwhile; ?>
		 </div>
	</div>
</div>