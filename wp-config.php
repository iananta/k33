<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'k33' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R,i%6ZL,9W(D*+@}&DisAK=nRD0S&Vm$=DZ|*,5^^!wX[!Z>V^]#0Zux!?/gqyvz' );
define( 'SECURE_AUTH_KEY',  '=v2 G$1Myh<?3L#(50VQmx8W{V?n^)L.Be)#]Y`_OlON@XzLYn.`+s ~<y=GQd9_' );
define( 'LOGGED_IN_KEY',    'fV21mbZqiti[y>jGz(39>y&.p8t1r?wlU?$6($=3j8okc[ern9T.0fGbaS<Lu:/h' );
define( 'NONCE_KEY',        '[,9tXMA6]ViXqhW?Epyr..aXQ])8Z}S3+6897ja/k|0-Rr.x6zgwLK8Ipp}p^t)c' );
define( 'AUTH_SALT',        'b>Ww{O3(f;=>{uv4*],Oc6p*As6y*#&P`< xs]0Iw5Hq+9l:v!a1)LnK5ivdH~h4' );
define( 'SECURE_AUTH_SALT', 'Dw;$3keY<uJcK|1+;?OfTO_Onps`8ZF(),tP[}Kf|p6Nv%Ds$hcF$W}lvQ;jj!#P' );
define( 'LOGGED_IN_SALT',   'j?`D,P&!d*f0$D)W1I5Jy*a]dS3qo;HA!hY(+z@tF9EXNf]$}=WrJ2SQkxBfS4#m' );
define( 'NONCE_SALT',       'WM_JF_HjR,ZpiGuchxVDJOo=&I9M,[DGZ}a]^E)A;f7zZ-DjRP Is6MvR;(Arz}x' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
